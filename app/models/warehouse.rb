
class Warehouse 
	attr_reader :dts_object
	
	def initialize
	  if Rails.env.production?
	   @dts_object = TinyTds::Client.new(:username => 'dhww', :password => '123queso', :host => '186.15.71.174',:database=>"HappyHill")
	  else
	   @dts_object = TinyTds::Client.new(:username => 'dhww', :password => '123queso', :host => '192.168.0.110',:database=>"HappyHill")  
	  end
	end

  def getNewReleases(top_cod_prod)
    res = []
    sql = "SELECT DISTINCT p.cod_prod FROM Prod p INNER JOIN BodPro b ON b.cod_prod = p.cod_prod WHERE b.cod_prod > '"+ top_cod_prod +"' AND b.cod_bod LIKE 'w%' ";
    result = @dts_object.execute(sql);
    i = 0
    result.each do |row|
      res[i] = row
      i = i + 1
    end
    res
  end
	
	def getDataCli(where)
		res = {}
		sql = "SELECT * FROM [Cli] WHERE " + where
		result = @dts_object.execute(sql)
		result.each do |row|
			res = row
		end		
		res
	end
	
	def getStockDetail(where)
		res = []
		sql = "SELECT * FROM [Prod] WHERE " + where;
		result = @dts_object.execute(sql);
		i = 0
		result.each do |row|
			res[i] = row
			i = i + 1
		end
		res
	end
	
	def getStock(where)
		res = []
		sql = "SELECT * FROM [Prod] WHERE " + where;
		result = @dts_object.execute(sql);
		i = 0
		result.each do |row|
			res[i] = row
			i = i + 1
		end
		res
	end
	
	def getProdInfo(strProdId)
		res = {}
		sql = "SELECT p.precio_u as precio, b.cant FROM Prod p INNER JOIN BodPro b ON b.cod_prod = p.cod_prod WHERE p.cod_prod = '" + strProdId + "' AND b.cod_bod LIKE 'w%'";
		result = @dts_object.execute(sql)
		result.each do |row|
			res = row
		end		
		res
	end
	
	def getProductTitle(cod_car5)
    res = {}
    sql = "SELECT * FROM [Carac05] WHERE Cod_Car5 = '" + cod_car5 + "'";    
    result = @dts_object.execute(sql)
    result.each do |row|
      res = row
    end
    res
  end
	
end