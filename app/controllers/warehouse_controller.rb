

class WarehouseController < ApplicationController
	include WashOut::SOAP

  soap_action "getNewReleases",
    :args => :string,
    :return => :string
  def getNewReleases
    top_cod_prod = @wr["getNewReleases"]["strLastCodProd"]   
    warehouse = Warehouse.new
    new_prods = warehouse.getNewReleases(top_cod_prod)
    render :soap=> new_prods.to_xml
  end

	soap_action "addProdToCart",
		:args => :string,
		:return => :string
  def addProdToCart
	render :soap => "bb"
  end

	soap_action "getStock",
		:args =>:string,
		:return => :string
  def getStock
	where = @wr["getStock"]["strWhere"]
	warehouse = Warehouse.new
	stock_detail = warehouse.getStockDetail(where)			
	render :soap=> stock_detail.to_xml
  end

	soap_action "getStockDetail",
		:args => :string,
		:return => :string
  def getStockDetail
	where = @wr["getStockDetail"]["strWhere"]
	warehouse = Warehouse.new
	stock_detail = warehouse.getStockDetail(where)			
	render :soap=> stock_detail.to_xml
  end

	soap_action "getCartInfo",
		:args => :string,
		:return => :string
  def getCartInfo
	render :soap => "ff"
  end

	soap_action "getProdInfo",
		:args => :string,
		:return => :string
  def getProdInfo
		prod_id = @wr["getProdInfo"]["strProdID"]
		warehouse = Warehouse.new
		prod_info = warehouse.getProdInfo(prod_id)			
		render :soap=> prod_info.to_xml
  end

	soap_action "acDataCli",
		:args => :string,
		:return => :string
  def acDataCli
	render :soap => "hh"
  end

	soap_action "getDataCli",
		:args=>:string,
		:return=>:string
	def getDataCli
		where = @wr["getDataCli"]["strWhere"]
		warehouse = Warehouse.new
		customer = warehouse.getDataCli(where)			
		render :soap=> customer.to_xml
	end

	before_filter :stamp_params_from_xml
	def stamp_params_from_xml	
		@wr = Hash.from_xml(params[:value])			
	end

end
