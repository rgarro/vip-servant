require "spec_helper"

describe WarehouseController do
  describe "routing" do
    
    it "warehouse/wsdl route to warehouse#_generate_wsdl" do 
      get("/warehouse/wsdl").should route_to("warehouse#_generate_wsdl")
    end
    
  end
end