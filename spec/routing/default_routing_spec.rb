require "spec_helper"

describe DefaultController do
  describe "routing" do
    
    it "root route to #index" do 
      get("/").should route_to("default#index")
    end
    
  end
end