require 'spec_helper'

describe "default/index.html.erb" do
	it "tshirt image on place holder" do
		render
		response.should have_tag("img",:with=>{:class=>'gnrtshirt'})
	end
end
