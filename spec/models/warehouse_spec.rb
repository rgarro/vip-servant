# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Warehouse do
	before :all do
		#keep calm, breathe deep
		@warehouse = Warehouse.new
	end
	
	it "We should have a warehouse type object ..." do
		@warehouse.should be_an_instance_of Warehouse
	end
	
	it "should have an accecible dts connection object ..." do
		@warehouse.should respond_to(:dts_object)
		@warehouse.dts_object.should be_an_instance_of TinyTds::Client
	end
	
	it "dts connection object should succesfully connect ..." do
		@warehouse.dts_object.active?.should be_true
	end
	
	it "should have a getDataCli method ..." do
		@warehouse.should respond_to(:getDataCli)
	end
	
	it "getDataCli method should return customer data ..." do
		where = "email='rgarro@gmail.com'"
		customer = @warehouse.getDataCli(where)		
		customer['email'].should eq('rgarro@gmail.com                                  ')		
	end
	
	it "should have getProductTitle method" do
	  @warehouse.should respond_to(:getProductTitle)
	end
	
	it "getProductTitle('4918-7-F')[Descrip] should return 'GOKÚ SUPER SAIYAN GOD' " do
    @warehouse.getProductTitle('4918-7-F')["Descrip"].should eq("GOKÚ SUPER SAIYAN GOD                                                                                                                                                                                                                                          ")
	end
	
	#it "getDataCli method should handle non exixting customers ..." do
		#where = "email='xxx@xxx.com'"
		#customer = @warehouse.getDataCli(where)
		#puts customer.class
		#puts customer.methods
		#puts customer.inspect		
	#end
end